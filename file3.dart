class MTN {
   var name;
   var category;
   var developer;
  var year;
  
  showMTNInfo(){
    print("App Name is : ${name}");
    print("Category of app is : ${category}");
    print("Developer of app is : ${developer}");
    print("Year app won is : ${year}");
  }
}
void main(){
  var app = new MTN();
  app.name= "Ambani Africa";
  app.category = "Education";
  app.developer = "Mukundi Lambani";
  app.year="2021";
  app.showMTNInfo();
}